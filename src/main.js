// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './../node_modules/jquery/dist/jquery.min.js';
import './../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './../node_modules/bootstrap/dist/js/bootstrap.min.js';

import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
const config = require('./firebase-config')

Vue.config.productionTip = false

let app;

firebase.initializeApp(config);
firebase.auth().onAuthStateChanged(function(user) {
    if (!app) {
        /* eslint-disable no-new */
        app = new Vue({
            el: '#app',
            template: '<App/>',
            components: { App },
            router
        })
    }
});