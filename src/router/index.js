import Vue from 'vue'
import Router from 'vue-router'

import Student from '@/components/Student/Student'
import AddStudent from '@/components/Student/AddStudent'
import Login from '@/components/Login'
import SignUp from '@/components/SignUp'
import Dashboard from '@/components/Dashboard'
import firebase from 'firebase'

Vue.use(Router)

let router = new Router({
    routes: [{
            path: '*',
            redirect: '/login'
        },
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/sign-up',
            name: 'SignUp',
            component: SignUp
        },
        {
            path: '/student',
            name: 'Student',
            component: Student,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/student/add',
            name: 'AddStudent',
            component: AddStudent,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        }
    ],
    linkActiveClass: "active"
})

router.beforeEach((to, from, next) => {
    let currentUser = firebase.auth().currentUser;
    let requiresAuth = to.matched.some(record => record.meta.requiresAuth);

    if (requiresAuth && !currentUser) next('login')
    else if (!requiresAuth && currentUser) next('student')
    else next()
})

export default router